#!/bin/bash

input_video=$1
output_filename=$2

if [ $# -ne 2 ]
then
    echo "Invalid number of arguments"
    exit 1
fi

if [ ! -e "$input_video" ] || [[ ! "$input_video" == *.mp4 ]];
then
    echo "Invalid input file"
    exit 1
fi

if [[ ! $output_filename == *.gif ]];
then
    echo "Output file must include .gif extension"
    exit 1
fi

total_seconds=`ffprobe -v error -select_streams v:0 -show_entries stream=duration -of \
                default=noprint_wrappers=1:nokey=1 $input_video`

two_third_seconds=$(echo $total_seconds*2/3 | bc)

ffmpeg -i $input_video -ss $two_third_seconds -t 8 -vf fps=2,scale=480:-1 $output_filename -y
