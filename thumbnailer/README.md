# Video Thumbnailer

A script which generates an animated gif from mp4

## How to run

```bash
docker build -t <image-name> .

docker run -v /path/to/video/:/app <image-name> make_thumbnail.sh sample.mp4 output.gif
```