import os
import json
import redis
from flask import Flask, jsonify, request, Response
import requests
from flask_cors import CORS

app = Flask(__name__)
CORS(app, resources={r"*": {"origins": os.getenv('WEBAPP', 'http://localhost:8000')}})

OBJECT_STORAGE_URL = 'http://sos:8080'

CONTENT_TYPE = 'application/json'

STATUS_OK = requests.codes['ok']
STATUS_BAD_REQUEST = requests.codes['bad_request']
STATUS_NOT_FOUND = requests.codes['not_found']
STATUS_NOT_ALLOWED = requests.codes['not_allowed']


class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    QUEUE_NAME = 'queue:factoring'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)


def push_job_to_redis(json_packed):
    RedisResource.conn.rpush(RedisResource.QUEUE_NAME, json_packed)


@app.route('/factor', methods=['POST'])
def post_factor_job():
    body = request.json
    json_packed = json.dumps(body)
    print('packed:', json_packed)
    RedisResource.conn.rpush(
        RedisResource.QUEUE_NAME,
        json_packed)
    
    return jsonify({'status': 'OK'})


@app.route('/<bucket_name>/<object_name>', methods=['POST'])
def post_create_gif_job(bucket_name, object_name):
    if 'submit' in request.args:
        resp = requests.get(f'{OBJECT_STORAGE_URL}/{bucket_name}/{object_name}?metadata')
        if resp.status_code == STATUS_OK:
            if '.mp4' not in object_name:
                return Response(status=STATUS_BAD_REQUEST)
            target_object = object_name.replace('.mp4', '.gif')
            job = {
                'bucket': bucket_name,
                'object': object_name,
                'target_object': target_object
            }
            push_job_to_redis(json.dumps(job))
            return Response(status=STATUS_OK)
        return Response(status=resp.status_code)
    return Response(status=STATUS_NOT_ALLOWED)


@app.route('/<bucket_name>', methods=['POST'])
def post_create_multiple_gif_job(bucket_name):
    if 'submit' in request.args and len(request.args) == 1:
        resp = requests.get(f'{OBJECT_STORAGE_URL}/{bucket_name}?list')
        if resp.status_code == STATUS_OK:
            resp_json = resp.json()
            objects = [obj for obj in resp_json.get('objects') if '.mp4' in obj.get('name')]
            if len(objects) == 0:
                body = json.dumps({'message': f'no mp4 found in {bucket_name}'})
                return Response(response=body, status=STATUS_BAD_REQUEST, content_type=CONTENT_TYPE)
            for obj in objects:
                object_name = obj.get('name')
                target_object = object_name.replace('.mp4', '.gif')
                requests.delete(f'{OBJECT_STORAGE_URL}/{bucket_name}/{target_object}?delete')
                job = {
                    'bucket': bucket_name,
                    'object': object_name,
                    'target_object': target_object
                }
                push_job_to_redis(json.dumps(job))
            return jsonify({'status': 'OK'})
        return Response(status=resp.status_code)
    return Response(status=STATUS_NOT_ALLOWED)


@app.route('/<bucket_name>')
def list_all_gif_from_bucket(bucket_name):
    if 'list' in request.args and len(request.args) == 1:
        resp = requests.get(f'{OBJECT_STORAGE_URL}/{bucket_name}?list')
        if resp.status_code == STATUS_OK:
            resp_json = resp.json()
            objects = [obj for obj in resp_json.get('objects') if '.gif' in obj.get('name')]
            resp_json['objects'] = objects
            return jsonify(resp_json)
        return Response(status=resp.status_code)
    return Response(status=STATUS_NOT_ALLOWED)


@app.route('/<bucket_name>/get_all_videos')
def list_all_videos_from_bucket(bucket_name):
    resp = requests.get(f'{OBJECT_STORAGE_URL}/{bucket_name}?list')
    if resp.status_code == STATUS_OK:
        resp_json = resp.json()
        objects = [obj for obj in resp_json.get('objects') if '.mp4' in obj.get('name')]
        resp_json['objects'] = objects
        return jsonify(resp_json)
    return Response(status=resp.status_code)


@app.route('/<bucket_name>/<gif_name>', methods=['DELETE'])
def delete_gif(bucket_name, gif_name):
    resp = requests.delete(f'{OBJECT_STORAGE_URL}/{bucket_name}/{gif_name}?delete')
    if resp.status_code == STATUS_OK:
        return jsonify({"status": "OK"})
    return Response(status=resp.status_code)


@app.route('/<bucket_name>', methods=['DELETE'])
def delete_all_gif(bucket_name):
    resp = requests.get(f'{OBJECT_STORAGE_URL}/{bucket_name}?list')
    if resp.status_code == STATUS_OK:
        resp_json = resp.json()
        objects = [obj.get('name') for obj in resp_json.get('objects') if '.gif' in obj.get('name')]
        for obj in objects:
            requests.delete(f'{OBJECT_STORAGE_URL}/{bucket_name}/{obj}?delete')
        return jsonify(resp_json)
    return Response(status=resp.status_code)
