import Vue from 'vue';
import Router from 'vue-router';
import ShowAllGifs from '@/views/ShowAllGifs';
import ShowAllVideos from '@/views/ShowAllVideos';
import NotFound from '@/views/NotFound';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/:bucketName/show_all_gifs',
      name: 'ShowAllGifs',
      component: ShowAllGifs,
    },
    {
      path: '/:bucketName/show_all_videos',
      name: 'ShowAllVideos',
      component: ShowAllVideos,
    },
    {
      path: '/not-found',
      name: 'NotFound',
      component: NotFound,
    },
  ],
});
