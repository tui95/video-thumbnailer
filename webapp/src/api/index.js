import axios from 'axios';

const API_URL = process.env.WEB_CONTROLLER ? process.env.WEB_CONTROLLER : 'http://localhost:5000';

const api = axios.create({
  baseURL: API_URL,
  timeout: 5000,
});

const createGif = (bucketName, objectName) => {
  const url = `${bucketName}/${objectName}?submit`;
  return api.post(url);
};

const createMultipleGif = (bucketName) => {
  const url = `${bucketName}?submit`;
  return api.post(url);
};

const listAllGif = (bucketName) => {
  const url = `${bucketName}?list`;
  return api.get(url)
    .then(({ data }) => data);
};

const listAllVideo = (bucketName) => {
  const url = `${bucketName}/get_all_videos`;
  return api.get(url)
    .then(({ data }) => data);
};

const deleteGif = (bucketName, gifName) => {
  const url = `${bucketName}/${gifName}`;
  return api.delete(url)
    .then(({ status }) => status);
};

const deleteAllGif = (bucketName) => {
  const url = `${bucketName}`;
  return api.delete(url);
};

export {
  createGif,
  createMultipleGif,
  listAllGif,
  listAllVideo,
  deleteGif,
  deleteAllGif,
};
